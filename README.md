# cfgmgmtv3

roles for:
* building packages
* building iso images
* installing llm tools
* configuring dev workstations
* configuring DAW workstations


## to install llm tools

```bash
ansible-playbook llmops.yml --tags chroma
ansible-playbook llmops.yml --tags agnai
ansible-playbook llmops.yml --tags flowise
ansible-playbook llmops.yml --tags llmstack
ansible-playbook llmops.yml --tags n8n
```
