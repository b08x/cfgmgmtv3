#!/usr/bin/env ruby
# frozen_string_literal: true

require 'cli/ui'
require 'yaml'
require 'json'
require 'csv'
require 'open4'
require "tty-prompt"


APP_ROOT = File.expand_path(__dir__)

PACKAGES = File.join(APP_ROOT, 'roles', 'packages')

CLI::UI::StdoutRouter.enable

CLI::UI::Frame.open('{{*}} {{bold:a}}', color: :green) do
  CLI::UI::Frame.open('{{i}} b', color: :magenta) do
    CLI::UI::Frame.open('{{?}} c', color: :cyan) do


  @distro = CLI::UI.ask('What language/framework do you use?', options: %w(AlmaLinux Archlinux))

    end
  end

CLI::UI::Frame.divider('{{v}} lol')

puts "#{@distro}"

packages = YAML.load_file(File.join(PACKAGES, 'vars', "#{@distro}.yml"))

prompt = TTY::Prompt.new

choice = prompt.multi_select("Select packages", packages["packages"])

p choice

end


`ansible-playbook packages.yml --tags "#{packages}" -e "ansible_distribution="#{distro}"`


# CLI::UI::Prompt.ask('What language/framework do you use?') do |handler|
#   handler.option('rails')  { |selection| selection }
#   handler.option('go')     { |selection| selection }
#   handler.option('ruby')   { |selection| selection }
#   handler.option('python') { |selection| selection }
# end
